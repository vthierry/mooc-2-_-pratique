# Fiche transversale

## Présentation de SNT

Dans le cadre de la journée porte ouverte de votre lycée, vous êtes chargé de la présentation de l’enseignement SNT. Vous allez accueillir les parents par groupe (environ 10 parents par groupe), les groupes s’enchainant environ toutes les 15 minutes. Quelle forme prendra votre présentation ? Rédiger le support que vous utiliserez au cours de cette présentation.

