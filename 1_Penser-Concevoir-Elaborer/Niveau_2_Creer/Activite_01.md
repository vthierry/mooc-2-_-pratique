# Traitement des données en table

Dans la section précédente, vous avez, lors de l'[activité 1](../Activite_01.md), analysé et critiqué une première activité sur les fichiers texte et les données en tables. Proposez une suite qui permettent de manipuler plus avant les données en table en utilisant la table introduite à la fin de l'activité 1 : les prénoms.

Précisez les objectifs, les pré-requis, le matériel, etc.
