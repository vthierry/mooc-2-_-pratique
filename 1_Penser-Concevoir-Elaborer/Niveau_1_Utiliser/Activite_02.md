# Programmation Python

La [fiche d'exercices _Turtle_](Ressources/Fiche_02/02_LangagesProg_FicheEleve01.pdf) propose un certain nombre d'exercices de programmation utilisant le module `turtle` de Python.

1. Quels sont les objectifs de cette fiche et à quelle partie du programme NSI fait-elle écho ? Réalisez un plan des éléments de programmation nécessaires à la réalisation des différents exercices. 
2. Quels sont les pré-requis ?
3. Proposez une coupe parmi les exercices et justifiez.
4. Discutez des variantes de cette fiche proposées dans le cadre de ce MOOC. 