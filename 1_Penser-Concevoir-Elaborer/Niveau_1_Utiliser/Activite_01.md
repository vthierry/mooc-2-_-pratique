# Les données au format texte

Le notebook [Activité sur les données en table (épisode I)](Ressources/Fiche01/01_DonneesEnTable_FicheEleve01.ipynb) propose une première activité pour manipuler les fichiers texte et les données au format csv. Cette fiche est censée enchaîner sur une suite (voir l'[activité 1 du niveau suivant](../Niveau_2_Creer/Activite_02.md))

1. La fiche commence par annoncer le but ; mais pouvez-vous précisez les objectifs d'une telle fiche (établir un lien avec le programme officiel peut-être) ?
2. Quels sont les pré-requis ?
3. Quelles sont les questions de cette fiche qui semblent peu appropriées ?
4. Proposer une version corrigée de cette fiche
5. Discuter des différentes variantes de cette fiche proposées dans le cadre de ce MOOC. 