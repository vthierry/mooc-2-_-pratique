# Module Mise en oeuvre des compétences professionnelles

Les activités dans les blocs permettent de mobiliser les [compétences professionnelles du professeur formateur](https://cache.media.eduscol.education.fr/file/30/30/3/perso4093_annexe1_452303.pdf) 

1. Maîtriser les savoirs disciplinaires et leur didactique
2. Maîriser la langue française dans le cadre de son enseignement
3. Construire, mettre en oeuvre et animer des situation d'enseignement et d'apprentissage prenant en compte la diversité des élèves
4. Organiser et assurer un mode de fonctionnement du groupe favorisant l'apprentissage et la socialisation des élèves
5. Évaluer les progrès et les acquisitions des élèves 

## Penser - Concevoir - Élaborer

### Niveau 1 -- Utiliser

- Identifier les objectifs (connaissances et compétences) d'une activité
- Identifier les pré-requis d'une activité
- Juger si une activité est appropriée ou pas 
- Adapter une activité

### Niveau 2 -- Créer

- Construire une activité élève de compréhension en utilisant des exercices appropriés au concept (en s'inspirant d'une activité existante ou à partir d'une idée)
- Construire une activité élève de production (en s'inspirant d'une activité existante ou à partir d'une idée)
- Construire une séquence

## Mettre en oeuvre - Animer

### Niveau 1

- Identifier et critiquer le scénario d'une séquence
- Scénariser une séquence existante

### Niveau 2

- Scénariser entièrement une séance, préciser notamment :
    - les différentes phases, durée,
    - l'organisation du tableau, écran, papier
    - l'organisation pédagogique (seul, binôme, groupe...)
    - les attendus
    - les interactions
    - l'activité du professeur
    - la place de la trace écrite

## Accompagner l'individu et le collectif

### Niveau 1

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance

### Niveau 2

- Proposer une activité de remédiation et d'approfondissement à partir d'une idée
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)

## Observer - Analyser - Évaluer

### Niveau 1

- Proposer un ensemble d'évaluations (courte en classe, devoir maison etc.) sur une ou plusieurs activités vues précédemment
- Proposer une évaluation conséquente type _Bac blanc_ sur une partie conséquente du programme   

### Niveau 2

- Proposer une grille d'évaluation pour un projet
- Proposer une présentation et des indicateurs pour l'institution et la famille 
